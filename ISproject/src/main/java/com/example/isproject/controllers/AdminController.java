package com.example.isproject.controllers;

import com.example.isproject.api.entities.AdminOperationSpec;
import com.example.isproject.internals.MyRestException;
import com.example.isproject.internals.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

@Controller
public class AdminController {

    @Autowired
    private UsersService usersService;

    @GetMapping("/admin")
    public ModelAndView getAdminPanelView() {
        return new ModelAndView("admin", "users", usersService.getAllUsers());
    }

    @PostMapping("/admin")
    public String updataUsers(MultipartHttpServletRequest req) {
        try {
            Set<AdminOperationSpec> specs = AdminOperationSpec.parseAdminOperationsRequest(req);
            if (specs.stream().anyMatch(s -> s.getUserId() == null)) {
                throw new MyRestException(HttpStatus.BAD_REQUEST, "User id is not specified for some operation");
            } else {
                usersService.performAdminOperations(specs);
            }
        } catch (MyRestException e) {
            throw e.wrapWithResponseStatusException("Admin operation cancelled");
        }
        return "redirect:/admin";
    }
}
