package com.example.isproject.controllers;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.isproject.db.entities.Role;
import com.example.isproject.api.entities.UserChangePasswordSpec;
import com.example.isproject.api.entities.UserRegisterSpec;
import com.example.isproject.internals.UsersService;

@Controller
@RequestMapping("/")
public class RootController {
    private final UsersService _userService;

    public RootController(UsersService userService) {
        _userService = userService;
    }

	/*
	@GetMapping("")
	public ResponseEntity<Object> resolveDefaultLocation(HttpServletRequest request){
		String acceptTypeString = request.getHeader(HttpHeaders.ACCEPT);
		if (acceptTypeString != null) {
			List<MimeType> expectedMimeTypes = MimeTypeUtils.parseMimeTypes(acceptTypeString);	
			if (expectedMimeTypes.stream().anyMatch(t -> t.equalsTypeAndSubtype(MimeTypeUtils.TEXT_HTML))) {
			return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("/dishes?view=html")).build();
			}
		}
		return ResponseEntity.status(HttpStatus.FOUND).location(URI.create("/dishes")).build();
	}
	*/

    @GetMapping("")
    public ModelAndView resolveDefaultLocation() {
        return new ModelAndView("index");
    }


    @GetMapping("/registration")
    public ModelAndView getRegistrationHtmlForm() {
        return new ModelAndView("registration", "userForm", new UserRegisterSpec());
    }


    @PostMapping("/registration")
    public ModelAndView registerNewUser(@ModelAttribute("userForm") @Valid UserRegisterSpec userForm, BindingResult bindingResult) {

        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
            bindingResult.rejectValue("password", "error.password.confirm-not-matched");
        }

        if (!bindingResult.hasErrors()) {
            if (_userService.registerNewUser(userForm.getUsername(), userForm.getPassword(), Role.USER)) {
                return new ModelAndView("redirect:/");
            } else {
                bindingResult.rejectValue("username", "error.user.already-exists");
            }
        }

        ModelAndView result = new ModelAndView("registration", "userForm", userForm);
        result.setStatus(HttpStatus.BAD_REQUEST);
        return result;

    }

    @GetMapping("/settings")
    public ModelAndView getUserAccountSettingsHtmlView() {
        return new ModelAndView("userAccountSettings", "userForm", new UserChangePasswordSpec());
    }


    @PostMapping("/settings")
    public ModelAndView changeUserAccountSettings(@ModelAttribute("userForm")
                                                  @Valid UserChangePasswordSpec userForm, BindingResult bindingResult) {
        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
            bindingResult.rejectValue("password", "error.password.confirm-not-matched");
        }
        if (!bindingResult.hasErrors()) {
            if (_userService.changeMyOwnPassword(userForm.getOldPassword(), userForm.getPassword())) {
                return new ModelAndView("redirect:/");
            } else {
                bindingResult.rejectValue("password", "error.user.invalid-old-password");
            }
        }
        ModelAndView result = new ModelAndView("userAccountSettings", "userForm", userForm);
        result.setStatus(HttpStatus.BAD_REQUEST);
        return result;
    }

}
