package com.example.isproject;

import com.example.isproject.internals.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	
	@Autowired
	private UsersService usersService;
	
	
	public WebSecurityConfig(UsersService usersService) {
		this.usersService = usersService;
	}
	
	@Override
	protected void configure(HttpSecurity httpSec) throws Exception{
		httpSec.csrf()
			.disable()
			.authorizeRequests()
				.antMatchers("/registration").not().fullyAuthenticated()
				.antMatchers("/admin/**").hasRole("ADMIN")
				.antMatchers("/dishes/**").fullyAuthenticated()
				.antMatchers("/","/resources/**","/index.html","/js/**","/currentUser").permitAll()
			.anyRequest().authenticated()
			.and()
				.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/",true)
				.permitAll()
			.and()
				.logout()
				.permitAll()
				.logoutSuccessUrl("/");
	}
	
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(usersService).passwordEncoder(bCryptPasswordEncoder());
	}
	@Bean
	public  BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
