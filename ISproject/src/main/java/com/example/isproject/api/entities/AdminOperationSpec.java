package com.example.isproject.api.entities;

import com.example.isproject.db.entities.Role;
import com.example.isproject.internals.MyRestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AdminOperationSpec {

    private Long userId;
    private String newPassword;
    public Boolean toDelete;
    private Role newRole;

    public AdminOperationSpec() {
        // do nothing
    }

    public AdminOperationSpec(Long userId, String newPassword, Boolean toDelete, Role newRole) {
        this.userId = userId;
        this.newPassword = newPassword;
        this.toDelete = toDelete;
        this.newRole = newRole;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public Boolean getToDelete() {
        return toDelete;
    }

    public void setToDelete(Boolean toDelete) {
        this.toDelete = toDelete;
    }

    public Role getNewRole() {
        return newRole;
    }

    public void setNewRole(Role newRole) {
        this.newRole = newRole;
    }


    private static interface AttributeSetter {
        void set(AdminOperationSpec spec, String value);
    }

    private static final HashMap<String, AttributeSetter> specAttributeSetters = new HashMap<String, AttributeSetter>() {{
        put("userId", (s, v) -> s.setUserId(Long.parseLong(v)));
        put("newPassword", AdminOperationSpec::setNewPassword);
        put("newRole", (s, v) -> s.setNewRole(Role.valueOf(v)));
        put("toDelete", (s, v) -> s.setToDelete(v != null && !v.isEmpty()));
    }};

    public static Set<AdminOperationSpec> parseAdminOperationsRequest(MultipartHttpServletRequest req) throws MyRestException {

        Pattern fieldNamePattern = Pattern.compile("^users\\[(?<index>\\d+)\\]\\.(?<attr>\\w+)$");
        try {
            return req.getParameterMap().entrySet().stream().map(e -> new Object() {
                private final Matcher m = fieldNamePattern.matcher(e.getKey());
                final boolean parsed = m.matches();
                final int index = Integer.parseInt(m.group("index"));
                final String attr = m.group("attr");
                final String value = String.join("", e.getValue());
            }).collect(Collectors.groupingBy(p -> p.index))
              .values().stream()
                .map(objects -> objects.stream()
                .reduce(new AdminOperationSpec(), (e, x) -> e.append(x.attr, x.value), AdminOperationSpec::combine))
                .collect(Collectors.toSet());
        } catch (Throwable ex) {
            throw new MyRestException(HttpStatus.BAD_REQUEST, "Failed to parse admin operation request", ex);
        }
    }

    private AdminOperationSpec combine(AdminOperationSpec b) {
        return null;
    }

    private AdminOperationSpec append(String attr, String value) {
        specAttributeSetters.get(attr).set(this, value);
        return this;
    }
}
