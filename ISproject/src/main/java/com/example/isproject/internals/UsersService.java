package com.example.isproject.internals;

import com.example.isproject.api.entities.AdminOperationSpec;
import com.example.isproject.db.entities.Role;
import com.example.isproject.db.entities.User;
import com.example.isproject.db.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class UsersService implements UserDetailsService {

    @Autowired
    private UsersRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public boolean registerNewUser(String username, String password, Role role) {
        Optional<User> oldUser = userRepository.findByUsername(username);
        if (oldUser.isPresent()) {
            return false;
        }
        String encodedPassword = passwordEncoder.encode(password);
        User newUser = new User(null, encodedPassword, username, role);

        userRepository.save(newUser);
        return true;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }
        return user.get();
    }


    public boolean isAdminNotExists(String adminUsername) {
        return userRepository.findAll((r, q, b) -> b.or(
            b.equal(r.get("role"), Role.ADMIN),
            b.equal(r.get("username"), adminUsername)
        )).isEmpty();
    }

    public boolean changeMyOwnPassword(String oldPassword, String newPassword) {
        Optional<User> userInfo = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (userInfo.isPresent()) {
            User user = userInfo.get();
            if (passwordEncoder.matches(oldPassword, user.getEncodedPassword())) {
                user.setEncodedPassword(passwordEncoder.encode(newPassword));
                userRepository.save(user);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void performAdminOperations(Set<AdminOperationSpec> specs) throws MyRestException {
        for (AdminOperationSpec spec : specs) {
            this.performAdminOperation(spec);
        }

    }

    public User performAdminOperation(AdminOperationSpec spec) throws MyRestException {
        Optional<User> userInfo = userRepository.findById(spec.getUserId());
        if (userInfo.isPresent()) {
            User user = userInfo.get();
            if (spec.getToDelete() != null && spec.getToDelete()) {
                userRepository.delete(user);
                return null;
            } else {
                if (spec.getNewPassword() != null && !spec.getNewPassword().isBlank()) {
                    user.setEncodedPassword(passwordEncoder.encode(spec.getNewPassword()));
                }
                if (spec.getNewRole() != null) {
                    user.setRole(spec.getNewRole());
                }
                return userRepository.save(user);
            }
        } else {
            throw new MyRestException(HttpStatus.FAILED_DEPENDENCY, "User with id" + spec.getUserId() + "not found");
        }
    }

    public Page<User> findAll(PageSpec spec) {
        return userRepository.findAllByOrderByIdAsc(spec.toPageRequest());
    }

    public Optional<User> findUserById(long id) {
        return userRepository.findById(id);
    }

    public boolean deleteIfExists(long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return true;
        } else {
            return false;
        }
    }

    public Optional<User> getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || auth.getName() == null || auth.getName().isEmpty()) {
            return Optional.empty();
        } else {
            return userRepository.findByUsername(auth.getName());
        }
    }


}
