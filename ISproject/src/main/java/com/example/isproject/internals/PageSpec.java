package com.example.isproject.internals;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PageSpec {
    public final boolean applied;
    public final int page, size;

    private PageSpec(boolean applied, int page, int size) {
        this.applied = applied;
        this.page = page;
        this.size = size;
    }

    public Pageable toPageRequest() {
        return this.applied ? PageRequest.of(page, size) : Pageable.unpaged();
    }

    public static PageSpec make(Integer pageParam, Integer sizeParam) {
        int page = pageParam == null ? 0 : pageParam;
        int size = sizeParam == null ? 0 : sizeParam;

        return new PageSpec(size > 0, page, size);
    }
}
