package com.example.isproject.db.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.isproject.db.entities.Dish;


@Repository
public interface DishesRepository extends JpaRepository<Dish, Long> {


}
