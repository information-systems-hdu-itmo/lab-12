package com.example.isproject.db.entities;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {

    ADMIN(0L),
    MANAGER(1L),
    USER(2L);


    public static class Names {

        private Names() {

        }

        public static final String ADMIN = "ROLE_ADMIN";
        public static final String MANAGER = "ROLE_MANAGER";
        public static final String USER = "ROLE_USER";
    }

    private final long id;

    private Role(long id) {
        this.id = id;
    }

    private long getId() {
        return this.id;
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + this.name();
    }
}
