<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Menu</title>
</head>
<body>
<div align="center">
	<h2>Menu</h2>
	
	<table border="1" cellpadding="5">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Price</th>
		<th>KCal</th>
		<th>Description</th>
		<th>ManagerId</th>
		<th>Image</th>
		<th>Actions</th>
	</tr>
	<c:forEach items="${dishes.items}" var="dish">
	<tr>
	 	<td>${dish.id}</td>
	 	<td>${dish.name}</td>
	 	<td>${dish.price}</td>
	 	<td>${dish.kcal}</td>
	 	<td>${dish.description}</td>
	 	<td>--</td><!-- TODO -->
	 	<td>
	 		<c:choose>
	 			<c:when test="${not empty dish.imageId}">
	 				<img src="/dishes/${dish.id}/image " width="100" height="100"/>
	 			</c:when>
	 			<c:otherwise>
	 				No image
	 			</c:otherwise>
	 		</c:choose>		
	 	</td>
	 	<sec:authorize access="isAuthenticated() and(hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN'))">
            <td>
                <a href="/dishes/${dish.id}?action=edit&mode=htmlForm">Edit</a>
                <a href="/dishes/${dish.id}?action=delete&mode=htmlForm">Delete</a>
            </td>
	 	</sec:authorize>
	</tr>
	</c:forEach>
	</table>
	<a href="/dishes?action=create&mode=htmlForm">Create dish</a>
</div>
</body>
</html>