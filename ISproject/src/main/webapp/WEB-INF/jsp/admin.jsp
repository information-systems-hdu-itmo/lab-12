<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head> 
<meta charset="ISO-8859-1">
<title>Admin panel</title>
</head>
<body>
	<form action="${pageContext.request.contextPath}/admin" method="POST" enctype="multipart/form-data">
	<table>
	<h1>Admin Panel</h1>
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Password</td>
			<td>Role</td>
			<td>Delete</td>
		</tr>
		<c:forEach items="${users}" var="user" varStatus="s">
		
			<tr>
				<td><input type="hidden" name="users[${s.index}].userId" value="${user.id}" />${user.id}</td>
				<td>${user.username}</td>
				<td><input type="password" name="users[${s.index}].newPassword" placeholder="new password"/></td>
				<td>
					<select name="users[${s.index}].newRole">
						<c:forEach items="${user.role.values()}" var="r">
							<option value="${r.name()}" ${user.role == r ? "selected" : "" }>${r.name()}</option>
						</c:forEach>
					</select>
				</td>
				<td><input type="checkbox" name="users[${s.index}].toDelete"/></td>
			</tr>
		
		</c:forEach>
	</table>
	
	<input class="button" type="submit" value="Apply"/>
	<h2><a href="/">Main page</a></h2>
	</form>

</body>
</html>