<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Account settings</title>
</head>
<body>
	<div>
		<f:form method="POST" modelAttribute="userForm">
			<h1>${pageContext.request.userPrincipal.name}</h1>
			<h3>Change password</h3>
			<f:input type="password" path="oldPassword" placeholder="Old password" autofocus="true" />
			<f:errors path="oldPassword" />
			<f:input type="password" path="password" placeholder="New password" />
			<f:errors path="password" />
			<f:input type="password" path="passwordConfirm" placeholder="Confirm your new password" />
			<input type="submit" value="Change password" />
		</f:form>
	</div>
</body>
</html>
