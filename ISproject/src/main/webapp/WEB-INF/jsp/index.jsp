<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Main</title>
</head>
<body>
<div>
	<h3>${pageContext.request.userPrincipal.name}</h3>
	<sec:authorize access="!isAuthenticated()">
		<h4><a href="/login">Login</a></h4>
		<h4><a href="/registration">Register</a></h4>
	</sec:authorize>
	
	<sec:authorize access="isAuthenticated()">
	    <h4><a href="/settings">Account settings</a></h4>
		<h4><a href="/logout">Logout</a></h4>
	</sec:authorize>
	
	<h4><a href="/dishes?view=html">Menu</a></h4>
	
	<sec:authorize access="isAuthenticated() and hasRole('ROLE_ADMIN')">
		<h4><a href="/admin">Admin panel</a></h4>
	</sec:authorize>

</div>
</body>
</html>